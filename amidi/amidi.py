#!/usr/bin/env python

"""Contains an example of midi input, and a separate example of midi output.

By default it runs the output example.
python midi.py --output
python midi.py --input

"""

import sys
import os
import time
import random
import getpass
import readchar

import pygame
import pygame.midi
from pygame.locals import *
from collections import deque

try:  # Ensure set available for output example
    set
except NameError:
    from sets import Set as set

c = 48
d = 50
e = 52
f = 53
g = 55
a = 57
b = 59
C = 60
D = 62
E = 64
F = 65
G = 67
A = 69
B = 71
c2 = 72

def print_device_info():
    pygame.midi.init()
    _print_device_info()
    pygame.midi.quit()

def _print_device_info():
    for i in range( pygame.midi.get_count() ):
        r = pygame.midi.get_device_info(i)
        (interf, name, input, output, opened) = r

        in_out = ""
        if input:
            in_out = "(input)"
        if output:
            in_out = "(output)"

        print ("%2i: interface :%s:, name :%s:, opened :%s:  %s" %
               (i, interf, name, opened, in_out))
        



def input_main(device_id = None):
    pygame.init()
    pygame.fastevent.init()
    event_get = pygame.fastevent.get
    event_post = pygame.fastevent.post

    pygame.midi.init()

    _print_device_info()


    if device_id is None:
        input_id = pygame.midi.get_default_input_id()
    else:
        input_id = device_id

    print ("using input_id :%s:" % input_id)
    i = pygame.midi.Input( input_id )

    pygame.display.set_mode((1,1))



    going = True
    while going:
        events = event_get()
        for e in events:
            if e.type in [QUIT]:
                going = False
            if e.type in [KEYDOWN]:
                going = False
            if e.type in [pygame.midi.MIDIIN]:
                print (e)

        if i.poll():
            midi_events = i.read(10)
            # convert them into pygame events.
            midi_evs = pygame.midi.midis2events(midi_events, i.device_id)

            for m_e in midi_evs:
                event_post( m_e )

    del i
    pygame.midi.quit()



def output_main(device_id = None):
    """Execute a musical keyboard example for the Church Organ instrument

    This is a piano keyboard example, with a two octave keyboard, starting at
    note F3. Left mouse down over a key starts a note, left up stops it. The
    notes are also mapped to the computer keyboard keys, assuming an American
    English PC keyboard (sorry everyone else, but I don't know if I can map to
    absolute key position instead of value.) The white keys are on the second
    row, TAB to BACKSLASH, starting with note F3. The black keys map to the top
    row, '1' to BACKSPACE, starting with F#3. 'r' is middle C. Close the
    window or press ESCAPE to quit the program. Key velocity (note
    amplitude) varies vertically on the keyboard image, with minimum velocity
    at the top of a key and maximum velocity at bottom.

    Default Midi output, no device_id given, is to the default output device
    for the computer.
    
    """

    # A note to new pygamers:
    #
    # All the midi module stuff is in this function. It is unnecessary to
    # understand how the keyboard display works to appreciate how midi
    # messages are sent.

    # The keyboard is drawn by a Keyboard instance. This instance maps Midi
    # notes to musical keyboard keys. A regions surface maps window position
    # to (Midi note, velocity) pairs. A key_mapping dictionary does the same
    # for computer keyboard keys. Midi sound is controlled with direct method
    # calls to a pygame.midi.Output instance.
    #
    # Things to consider when using pygame.midi:
    #
    # 1) Initialize the midi module with a to pygame.midi.init().
    # 2) Create a midi.Output instance for the desired output device port.
    # 3) Select instruments with set_instrument() method calls.
    # 4) Play notes with note_on() and note_off() method calls.
    # 5) Call pygame.midi.Quit() when finished. Though the midi module tries
    #    to ensure that midi is properly shut down, it is best to do it
    #    explicitly. A try/finally statement is the safest way to do this.
    #
    GRAND_PIANO = 0
    CHURCH_ORGAN = 19

    instrument = GRAND_PIANO
    #instrument = GRAND_PIANO
    start_note = 53  # F3 (white key note), start_note != 0
    n_notes = 24  # Two octaves (14 white keys)
    
    bg_color = Color('slategray')


    pygame.init()
    pygame.midi.init()

    _print_device_info()

    if device_id is None:
        port = pygame.midi.get_default_output_id()
    else:
        port = device_id

    #print ("using output_id :%s:" % port)

    midi_out = pygame.midi.Output(port, 0)

    delay = .2
    score = deque(maxlen=10)
    s = 0
    last_note = 0

    while 1:

        note, velocity = 48, 100
        #midi_out.note_on(48, velocity)#C
        #midi_out.note_on(52, velocity)#E
        #midi_out.note_on(55, velocity)#G
        #time.sleep(delay)
        #midi_out.note_off(48, 0)#C
        #midi_out.note_off(52, 0)#E
        #midi_out.note_off(55, 0)#G


        #midi_out.note_on(53, velocity)#F
        #midi_out.note_on(57, velocity)#A
        #midi_out.note_on(48, velocity)#C
        #time.sleep(delay)
        #midi_out.note_off(53, 0)#F
        #midi_out.note_off(57, 0)#A
        #midi_out.note_off(48, 0)#C


        #midi_out.note_on(55, velocity)#G
        #midi_out.note_on(47, velocity)#B
        #midi_out.note_on(50, velocity)#D
        #time.sleep(delay)
        #midi_out.note_off(55, 0)#G
        #midi_out.note_off(47, 0)#B
        #midi_out.note_off(50, 0)#D


        #midi_out.note_on(48, velocity)#C
        #midi_out.note_on(52, velocity)#E
        #midi_out.note_on(55, velocity)#G
        #time.sleep(delay)
        #midi_out.note_off(48, 0)#C
        #midi_out.note_off(52, 0)#E
        #midi_out.note_off(55, 0)#G

        #rand = [48, 50, 52, 53, 55, 57, 59, 60, 62, 64, 65, 67, 69, 71]
        rand = [[c], [d], [e], [f], [g], [a], [b], [C], [D], [E], [F], [G], [A], [B], [c2], [c,e,g], [d,f,a], [e,g,b], [f,a,C], [g,b,D], [a,C,E], [b,D,F], [C,E,G],[D,F,A],[E,G,B], [F,A,c2]]
        

        note = random.choice(rand)
        resp =''
        #Single note
        if len(note) == 1:
            midi_out.note_on(note[0], velocity)#random
            time.sleep(delay*2)
            midi_out.note_off(note[0], 0)#random
            resp=note[0] 
        else:
            #Chord
            for member in note:
                midi_out.note_on(member, velocity)
            time.sleep(delay*2)
            for member in note:
                midi_out.note_off(member, 0)#random
            if note[0] == c:
                #resp = '\x03'
                resp = c
            elif note[0] == d:
                #resp = '\x04' 
                resp = d
            elif note[0] == e:
                #resp = '\x05' 
                resp = e
            elif note[0] == f:
                #resp = '\x06'
                resp = f
            elif note[0] == g:
                #resp = '\x07'
                resp = g
            elif note[0] == a:
                #resp = '\x01'
                resp = a
            elif note[0] == b:
                #resp = '\x02'
                resp = b
            elif note[0] == C:
                resp = C
            elif note[0] == D:
                resp = D
            elif note[0] == E:
                resp = E

        #print(note)

        ans = 0
        #print ("c d e f g a b C")
        n = readchar.readkey()
        if (n == 'c') or (n == ','):
            ans = 48
        elif (n == 'd') or (n == 'k'):
            ans = 50
        elif (n == 'e') or (n == 'i'):
            ans = 52
        elif (n == 'f') or (n == 'j'):
            ans = 53
        elif (n == 'g') or (n == 'h'):
            ans = 55
        elif (n == 'a'):
            ans = 57
        elif (n == 'b') or (n == 'n'):
            ans = 59
        elif (n == 'C') or (n == '<'):
            ans = 60
        elif (n == 'D') or (n == 'K'):
            ans = 62
        elif (n == 'E') or (n == 'I'):
            ans = 64
        elif (n == 'F') or (n == 'J'):
            ans = 65
        elif (n == 'G') or (n == 'H'):
            ans = 67
        elif (n == 'A') or (n == ':'):
            ans = 69
        elif (n == 'B') or (n == 'N'):
            ans = 71
        elif n ==  '\x03':
            ans = 72
        else:
            ans = n
            print ans

        os.system('clear')
        if ans == resp:
            print 1
            score.append(1)
        else: 
            print 0
            score.append(0)
        s = sum(score)%10
        if sum(score) == 10:
            print 100
        else:
            print s

        now = time.localtime(time.time())
        time1 = now[5]
        now = time.localtime(time.time())
        time2 = now[5]
        last_note = note

        time.sleep(.1)




#def play_note():

def usage():
    print ("--input [device_id] : Midi message logger")
    print ("--output [device_id] : Midi piano keyboard")
    print ("--list : list available midi devices")

def main(mode='output', device_id=None):
    """Run a Midi example

    Arguments:
    mode - if 'output' run a midi keyboard output example
              'input' run a midi event logger input example
              'list' list available midi devices
           (default 'output')
    device_id - midi device number; if None then use the default midi input or
                output device for the system

    """

    if mode == 'input':
        input_main(device_id)
    elif mode == 'output':
        output_main(device_id)
    elif mode == 'list':
        print_device_info()
    else:
        raise ValueError("Unknown mode option '%s'" % mode)
                
if __name__ == '__main__':

    try:
        device_id = int( sys.argv[-1] )
    except:
        device_id = None

    if "--input" in sys.argv or "-i" in sys.argv:

        input_main(device_id)

    elif "--output" in sys.argv or "-o" in sys.argv:
        output_main(device_id)
    elif "--list" in sys.argv or "-l" in sys.argv:
        print_device_info()
    else:
        usage()
