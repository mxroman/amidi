#!/usr/bin/env python

"""Contains an example of midi input, and a separate example of midi output.

By default it runs the output example.
python midi.py --output
python midi.py --input

"""

import sys
import os
import time
import random
import getpass
import readchar

import pygame
import pygame.midi
from pygame.locals import *
from collections import deque


n = readchar.readkey()
print(n)
if n == '\x03':
    print '1'
print(n)

